(TeX-add-style-hook
 "chimera"
 (lambda ()
   (LaTeX-add-labels
    "eq:abrams"
    "fig:abrams"
    "eq:chimera"
    "eq:metastability"))
 :latex)

